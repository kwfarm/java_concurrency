package kr.kwfarm.study.java.concurrency;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
public class ThreadDemo {
    @BeforeEach
    void beforeEach() {
        log.info("==========[{} Main Start]==========", Thread.currentThread().getName());
    }

    @AfterEach
    void afterEach() throws InterruptedException {
        log.info("==========[{} Main End]==========", Thread.currentThread().getName());
        TimeUnit.MICROSECONDS.sleep(200);
    }

    @Test
    void run() throws InterruptedException {
        List<String> returnNames = new ArrayList<>();
        new CustomThread(returnNames).run();
        Assertions.assertEquals("main", returnNames.get(0));
    }

    @Test
    void start() throws InterruptedException {
        List<String> returnNames = new ArrayList<>();
        new CustomThread("Thread-1", returnNames).start();
        TimeUnit.MICROSECONDS.sleep(100);
        Assertions.assertEquals("Thread-1", returnNames.get(0));
    }

    @Test
    void customName() throws InterruptedException {
        List<String> returnNames = new ArrayList<>();
        new CustomThread("foo", returnNames).start();
        TimeUnit.MICROSECONDS.sleep(100);
        Assertions.assertEquals("foo", returnNames.get(0));
    }

    @Test
    void interruptedTest() throws InterruptedException {
        InterruptedThread interruptedThread = new InterruptedThread();
        interruptedThread.start();
        TimeUnit.MICROSECONDS.sleep(10);

        assertFalse(interruptedThread.isInterrupted());
        interruptedThread.interrupt();
        assertTrue(interruptedThread.isInterrupted());
    }

    @Test
    void interruptedExceptionTest() throws InterruptedException {
        ArrayList<Exception> exceptions = new ArrayList<>();
        InterruptedExceptionThread thread = new InterruptedExceptionThread(exceptions);
        thread.start();
        TimeUnit.MICROSECONDS.sleep(200);

        assertFalse(thread.isInterrupted());
        thread.interrupt();
        assertTrue(thread.isInterrupted());
        TimeUnit.MICROSECONDS.sleep(100);
        assertTrue(exceptions.get(0) instanceof InterruptedException);
    }

    @Slf4j
    static class CustomThread extends Thread {
        private List<String> returnThreadName;

        public CustomThread(List<String> returnThreadName) {
            this.returnThreadName = returnThreadName;
        }

        public CustomThread(String threadName, List<String> returnThreadName) {
            super(threadName);
            this.returnThreadName = returnThreadName;
        }

        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            this.returnThreadName.add(threadName);
            log.info("{} Thread Running", threadName);
        }
    }

    @Slf4j
    static class InterruptedThread extends Thread {
        @Override
        public void run() {
            try {
                while(Thread.currentThread().isInterrupted() == false) {
                    log.info("Thread not Interrupted");
                }

                log.info("Thread Interrupted");
            } catch(Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    @Slf4j
    static class InterruptedExceptionThread extends Thread {
        private List<Exception> exceptions;

        public InterruptedExceptionThread(List<Exception> exceptions) {
            this.exceptions = exceptions;
        }

        @Override
        public void run() {
            try {
                for (int i = 0; i < 10; i++) {
                    TimeUnit.MICROSECONDS.sleep(50);
                }
            } catch(Exception e) {
                exceptions.add(e);
                log.error(e.getMessage(), e);
            }
        }
    }
}
